package APPLICATION;

import data.*;
import maker.DecideOn;
import maker.ProducerMaker;
import maker.ProducerMakerStandart;

public class Program {

    public static void main(String[] args) {

        //    ::: Model alan Maker bize compile time da cast edebilen Model instance'ı verir.

        //    - Burda enum type ile Model Impl paketlenir ve üretim Maker içinde tamamlanır.
        ProducerMaker<Product> producerMaker1 = new ProducerMaker<>();

        Product product = producerMaker1.makeCustomer(DecideOn.PRODUCT);

        //String price = product.getPriceOfProduct();

        //System.out.println("product ::: price: " + price);

        product.writeConsoleLine();


        ProducerMaker<Customer> producerMaker2 = new ProducerMaker<>();

        Customer customer = producerMaker2.makeCustomer(DecideOn.CUSTOMER);

        //String phone = customer.customerPhone();

        //System.out.println("customer ::: phone: " + phone);

        customer.writeConsoleLine();


        //   ::: Yine compile time da cast edilebilen bir Maker ile Model instance'ları alırız.

        //    - Bu defa yalnızca Class objeleri verilerek, Maker tarafından Model Impl instance'ları üretilir.
        ProducerMakerStandart<Product> producerMakerStandart1 = new ProducerMakerStandart<>();

        Product product2 = producerMakerStandart1.of(ProductImpl.class);

        //String price2 = product2.getPriceOfProduct();

        //System.out.println("product2 ::: price2: " + price2);

        product2.writeConsoleLine();

        ProducerMakerStandart<Customer> producerMakerStandart2 = new ProducerMakerStandart<>();

        Customer customer2 = producerMakerStandart2.of(CustomerImpl.class);

        //String phone2 = customer2.customerPhone();

        //System.out.println("customer2 ::: phone2: " + phone2);

        customer2.writeConsoleLine();

    }
}
