package maker;

import data.Customer;
import data.CustomerImpl;
import data.ProductImpl;

public enum DecideOn {

    CUSTOMER(CustomerImpl.class),

    PRODUCT(ProductImpl.class);

    private Class<?> classType;

    DecideOn(Class<?> classType) {

        this.classType = classType;
    }

    public Class<?> getClassType() {
        return classType;
    }
}
