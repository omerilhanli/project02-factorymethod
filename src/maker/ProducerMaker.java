package maker;

import data.BaseObject;
import data.Customer;

public class ProducerMaker<T extends BaseObject> {

    public T makeCustomer(DecideOn decide) {

        try {

            T t = (T) Class.forName(decide.getClassType().getName()).getConstructor().newInstance();

            return t;

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }
}
