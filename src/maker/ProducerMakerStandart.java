package maker;

public class ProducerMakerStandart<T> {

    public T of(Class<?> classType) {

        try {

            T t = (T) Class.forName(classType.getName()).getConstructor().newInstance();

            return t;

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }
}
