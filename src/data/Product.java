package data;

public interface Product extends BaseObject {

    String getPriceOfProduct();
}
